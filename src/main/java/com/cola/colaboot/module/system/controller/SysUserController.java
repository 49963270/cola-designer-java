package com.cola.colaboot.module.system.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.module.system.service.SysUserService;
import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @GetMapping("/pageList")
    public Res<?> queryPageList(SysUser sysUser,
                @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                @RequestParam(name="pageSize", defaultValue="10") Integer pageSize) {
        return Res.ok(sysUserService.pageList(sysUser,pageNo,pageSize));
    }

    @PostMapping("/saveOrUpdate")
    public Res<?> saveOrUpdate(SysUser sysUser) {
        sysUserService.saveOrUpdate(sysUser);
        return Res.ok("success");
    }

    @GetMapping("/info")
    public Res<?> getMyInfo(){
        return Res.ok(sysUserService.getByUsername("admin"));
    }
}
