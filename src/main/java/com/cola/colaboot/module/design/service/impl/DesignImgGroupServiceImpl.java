package com.cola.colaboot.module.design.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cola.colaboot.module.design.mapper.DesignImgGroupMapper;
import com.cola.colaboot.module.design.pojo.DesignImgGroup;
import com.cola.colaboot.module.design.service.DesignImgGroupService;
import com.cola.colaboot.module.system.pojo.SysUser;
import org.springframework.stereotype.Service;

@Service
public class DesignImgGroupServiceImpl extends ServiceImpl<DesignImgGroupMapper, DesignImgGroup> implements DesignImgGroupService {
    @Override
    public IPage<DesignImgGroup> pageList(DesignImgGroup group, Integer pageNo, Integer pageSize) {
        LambdaQueryWrapper<DesignImgGroup> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(StringUtils.isNotBlank(group.getGroupName()),DesignImgGroup::getGroupName,group.getGroupName());
        queryWrapper.orderByDesc(DesignImgGroup::getCreateTime);
        Page<DesignImgGroup> page = new Page<>(pageNo, pageSize);
        return page(page, queryWrapper);
    }
}
