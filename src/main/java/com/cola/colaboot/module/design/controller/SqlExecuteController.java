package com.cola.colaboot.module.design.controller;

import com.cola.colaboot.config.dto.Res;
import com.cola.colaboot.config.exception.ColaException;
import com.cola.colaboot.module.design.mapper.SqlExecuteMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/sql")
public class SqlExecuteController {

    @Autowired
    private SqlExecuteMapper mapper;

    @GetMapping("/executeSelect")
    public Res<?> executeSelect(String sql, boolean isArray){
        if (!sql.toUpperCase().startsWith("SELECT")){//防sql注入验证，目前只做了简单的字符串判断
            throw new ColaException("sql错误");
        }else{
            if(isArray){
                return Res.ok("success",mapper.executeList(sql));
            }else{
                return Res.ok("success",mapper.executeObject(sql));
            }
        }

    }
}
